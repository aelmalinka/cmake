# Copyright 2019 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

include(CheckCCompilerFlag)
include(CheckCXXCompilerFlag)

# 2019-12-07 AMR TODO: CMake to support c18
#set(CMAKE_C_STANDARD 18)
#set(CMAKE_C_EXTENSIONS OFF)
#set(CMAKE_C_STANDARD_REQUIRED ON)

if(NOT "${REQUESTED_CXX_STANDARD}")
	set(REQUESTED_CXX_STANDARD 20)
endif(NOT "${REQUESTED_CXX_STANDARD}")

set(CMAKE_CXX_STANDARD ${REQUESTED_CXX_STANDARD})
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_compile_options("$<IF:$<C_COMPILER_ID:MSVC>,/W4,-Wall>")
add_compile_options("$<$<NOT:$<C_COMPILER_ID:MSVC>>:-Wextra>")
add_compile_options("$<$<NOT:$<C_COMPILER_ID:MSVC>>:-pedantic>")

if(NOT NO_WERROR)
add_compile_options("$<$<NOT:$<C_COMPILER_ID:MSVC>>:-Werror>")
endif(NOT NO_WERROR)

configure_file(
	"${PROJECT_SOURCE_DIR}/config.h.in"
	"${PROJECT_BINARY_DIR}/config.h"
)
include_directories("${PROJECT_BINARY_DIR}")
add_definitions(-DHAVE_CONFIG_H)
