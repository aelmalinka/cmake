# Copyright 2019 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

include(CMakePackageConfigHelpers)

configure_package_config_file(${PROJECT_NAME}.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
	INSTALL_DESTINATION
		cmake/${PROJECT_NAME}/${PROJECT_NAME}Config.cmake
	PATH_VARS CMAKE_INSTALL_INCLUDEDIR CMAKE_INSTALL_LIBDIR
	INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}"
)
write_basic_package_version_file(
	${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Version.cmake
	VERSION ${PROJECT_VERSION}
	COMPATIBILITY SameMajorVersion
)
install(
	FILES
		${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
		${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Version.cmake
	DESTINATION
		${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
)

foreach(lib ${INSTALL_LIBS})
	install(
		TARGETS ${lib}
		EXPORT ${lib}
		LIBRARY
			COMPONENT Libraries
			NAMELINK_COMPONENT
		INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}
	)
#	2019-12-19 AMR TODO: this seems bad
	install(
		DIRECTORY src/
		DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}
		FILES_MATCHING PATTERN "*.hh"
	)
	install(
		EXPORT ${lib}
		NAMESPACE ${PROJECT_NAME}::
		DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
	)
endforeach()

foreach(bin ${INSTALL_BINS})
	install(
		TARGETS ${bin}
		RUNTIME
		DESTINATION ${CMAKE_INSTALL_BINDIR}
	)
endforeach()
