# Copyright 2019 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

include(GoogleTest)

# 2019-12-19 AMR NOTE: this needs to be before add_subdir or will be ignored when BUILD_GMOCK is true
set(INSTALL_GTEST OFF CACHE BOOL "install gtest" FORCE)

# 2019-10-23 AMR TODO: should this be an external project instead?

enable_testing()
add_subdirectory("${PROJECT_SOURCE_DIR}/ext/gtest" "${PROJECT_BINARY_DIR}/ext/gtest")
# 2019-12-19 AMR TODO: optional gmock
set(GTEST_LIBS gtest gmock gmock_main)

# 2019-09-02 AMR TODO: more?
mark_as_advanced(
	BUILD_GMOCK BUILD_GTEST BUILD_SHARED_LIBS INSTALL_GTEST
	gmock_build_tests gtest_build_samples gtest_build_tests
	gtest_disable_pthreads gtest_force_shared_crt gtest_hide_internal_symbols
)

set_target_properties(gtest PROPERTIES FOLDER ext)
set_target_properties(gtest_main PROPERTIES FOLDER ext)
set_target_properties(gmock PROPERTIES FOLDER ext)
set_target_properties(gmock_main PROPERTIES FOLDER ext)

macro(add_gtest target cxxfile)
	add_executable(${target} ${cxxfile})
	target_link_libraries(${target} ${GTEST_LIBS} ${TEST_LIBS})
	set_target_properties(${target} PROPERTIES FOLDER tests)
	gtest_discover_tests(${target})
endmacro(add_gtest)
