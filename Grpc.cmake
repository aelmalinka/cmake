# Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

find_package(Protobuf REQUIRED)
find_package(gRPC REQUIRED)

if(NOT "${PROTO_DEST_DIR}")
	set(PROTO_DEST_DIR "proto")
endif(NOT "${PROTO_DEST_DIR}")

file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}")

if(NOT "${PROTO_SOURCES}")
	file(GLOB PROTO_SOURCES "${PROTO_SRC_DIR}/*.proto")
	message(STATUS "Found protocols ${PROTO_SOURCES}")
endif(NOT "${PROTO_SOURCES}")

find_program(_PROTOBUF_PROTOC protoc)
find_program(_GRPC_CPP_PLUGIN_EXECUTABLE grpc_cpp_plugin)

foreach(FIL ${PROTO_SOURCES})
	message(STATUS "Protocol file ${FIL}")
	get_filename_component(fn "${FIL}" NAME)
	string(REGEX REPLACE \.proto$ "" fb "${fn}")

	list(APPEND PROTO_HDRS "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}/${fb}.pb.hh")
	list(APPEND PROTO_HDRS "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}/${fb}.grpc.pb.hh")
	list(APPEND PROTO_SRCS "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}/${fb}.pb.cc")
	list(APPEND PROTO_SRCS "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}/${fb}.grpc.pb.cc")

	add_custom_command(
		OUTPUT
			"${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}/${fb}.pb.cc"
		BYPRODUCTS
			"${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}/${fb}.pb.hh"
			"${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}/${fb}.grpc.pb.hh"
			"${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}/${fb}.grpc.pb.cc"
		COMMAND "${_PROTOBUF_PROTOC}"
		ARGS --grpc_out "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}"
			--cpp_out "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DEST_DIR}"
			-I "${CMAKE_CURRENT_SOURCE_DIR}/${PROTO_SRC_DIR}"
			--plugin=protoc-gen-grpc="${_GRPC_CPP_PLUGIN_EXECUTABLE}"
			"${CMAKE_CURRENT_SOURCE_DIR}/${PROTO_SRC_DIR}/${fn}"
		MAIN_DEPENDENCY "${CMAKE_CURRENT_SOURCE_DIR}/${PROTO_SRC_DIR}/${fn}"
	)
endforeach(FIL)

set_source_files_properties(${PROTO_SRCS} ${PROTO_HDRS} PROPERTIES GENERATED TRUE)
