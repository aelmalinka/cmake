# Copyright 2019 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

include(CheckCXXCompilerFlag)
include(CheckCCompilerFlag)

option(DEBUG "debug" OFF)

# 2019-09-01 AMR TODO: windows?
# 2019-10-22 AMR TODO: is this rechecking flags or caching
set(DEBUG_OPTIONS DEBUG_GFULL DEBUG_G3OG DEBUG_GOG DEBUG_G3 DEBUG_G)
set(DEBUG_GFULL -g3 -Og -gdwarf-5)
set(DEBUG_G3OG -g3 -Og)
set(DEBUG_GOG -g -Og)
set(DEBUG_G3 -g3)
set(DEBUG_G -g)

if(NOT "${DEBUG_FLAGS}")
	foreach(opt IN LISTS DEBUG_OPTIONS)
		set(prev TRUE)
		foreach(flag IN LISTS ${opt})
			check_c_compiler_flag(${flag} ${opt}_${flag}_C)
			check_cxx_compiler_flag(${flag} ${opt}_${flag}_CXX)
			if(${prev} AND ${${opt}_${flag}_C} AND ${${opt}_${flag}_CXX})
				set(prev TRUE)
			else()
				set(prev FALSE)
				unset(C)
				unset(CXX)
				break()
			endif()
			unset(C)
			unset(CXX)
		endforeach()

		if(prev)
			string(REPLACE ";" " " DBG_STR "${${opt}}")
			set(DEBUG_FLAGS "${DBG_STR}" CACHE STRING "debug flags")
			unset(prev)
			break()
		endif()
		unset(prev)
	endforeach()
endif()

if(DEBUG)
	set(CMAKE_VERBOSE_MAKEFILE TRUE)
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${DEBUG_FLAGS}")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${DEBUG_FLAGS}")
endif(DEBUG)
